package ngocvuthe.asm3.android.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

import ngocvuthe.asm3.android.model.Alarm;

import static android.content.ContentValues.TAG;

public class DatabaseManager extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "alarm_app";
    private static final String TABLE_NAME = "alarm";
    private static final String COL_ID = "id";
    private static final String COL_HOUR = "hour";
    private static final String COL_MINUTE = "minute";
    private static final String COL_FLAG = "flag";

    public DatabaseManager(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = "CREATE TABLE " + TABLE_NAME + "("
                + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL DEFAULT 1 ,"
                + COL_HOUR + " INTEGER,"
                + COL_MINUTE + " INTEGER,"
                + COL_FLAG + " INTEGER" + ")";

        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(" DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    // Create Alarm to Database
    public void insert(Alarm alarm) {
        SQLiteDatabase db = null;

        try {
            db = this.getWritableDatabase();

            ContentValues values = new ContentValues();

            values.put(COL_HOUR, alarm.getHour());
            values.put(COL_MINUTE, alarm.getMinute());
            values.put(COL_FLAG, alarm.getFlag());

            db.insert(TABLE_NAME, null, values);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }

    // Update Alarm
    public void update(Alarm alarm) {
        SQLiteDatabase db = null;

        try {
            db = this.getWritableDatabase();

            ContentValues values = new ContentValues();

            values.put(COL_ID, alarm.getId());
            values.put(COL_HOUR, alarm.getHour());
            values.put(COL_MINUTE, alarm.getMinute());
            values.put(COL_FLAG, alarm.getFlag());

            db.update(TABLE_NAME, values, COL_ID + " = " + alarm.getId(), null);
            Log.e("hi", db.toString());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }

    // Delete Alarm
    public void delete(int alarmId) {
        SQLiteDatabase db = null;

        try {
            db = this.getWritableDatabase();
            db.delete(TABLE_NAME, COL_ID + " = " + alarmId, null);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }

    // get all Alarm from Database
    public ArrayList<Alarm> getAlarmList() {

        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<Alarm> alarmArrayList = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(selectQuery, null);

        try {
            if (cursor.moveToFirst()) {
                do {
                    Alarm alarm = new Alarm(
                            cursor.getInt(0),
                            cursor.getInt(1),
                            cursor.getInt(2),
                            cursor.getInt(3));

                    alarmArrayList.add(alarm);

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, "getAlarmList: exception cause " + e.getCause() + " message " + e.getMessage());
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return alarmArrayList;
    }
}

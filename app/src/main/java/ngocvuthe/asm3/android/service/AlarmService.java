package ngocvuthe.asm3.android.service;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.IBinder;
import android.provider.Settings;

import ngocvuthe.asm3.android.receiver.AlarmReceiver;

public class AlarmService extends Service {
    MediaPlayer mediaPlayer; // this object to manage media

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String on_Off = intent.getExtras().getString("ON_OFF");
        switch (on_Off) {
            case "ADD":
                Uri uri = Settings.System.DEFAULT_ALARM_ALERT_URI;

                mediaPlayer = MediaPlayer.create(this, uri);

                mediaPlayer.start();
                break;
            case "OFF":
                int alarmId = intent.getExtras().getInt("AlarmId");

                if (mediaPlayer != null && mediaPlayer.isPlaying() && alarmId == AlarmReceiver.pendingId) {
                    // stop media
                    mediaPlayer.stop();
                    // reset it
                    mediaPlayer.reset();
                }
                break;
        }
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // turn off Alarm music
        mediaPlayer.stop();
        mediaPlayer.reset();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }
}

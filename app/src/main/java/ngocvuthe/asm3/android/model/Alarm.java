package ngocvuthe.asm3.android.model;

import java.io.Serializable;

public class Alarm implements Serializable {
    private int id;
    private int hour;
    private int minute;
    private int flag;

    public Alarm(int id, int hour, int minute, int flag) {
        this.id = id;
        this.hour = hour;
        this.minute = minute;
        this.flag = flag;
    }

    public Alarm(int hour, int minute, int flag) {
        this.hour = hour;
        this.minute = minute;
        this.flag = flag;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }
}

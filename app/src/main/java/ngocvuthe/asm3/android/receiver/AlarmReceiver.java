package ngocvuthe.asm3.android.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import ngocvuthe.asm3.android.service.AlarmService;


public class AlarmReceiver extends BroadcastReceiver {

    public static int pendingId;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent != null) {
            // this hold information to service
            Intent intentToService = new Intent(context, AlarmService.class);
            try {
                // getting intent key "intentType"
                String intentType = intent.getExtras().getString("intentType");
                switch (intentType) {
                    case "ADD_INTENT":
                        // assign pendingId
                        pendingId = intent.getExtras().getInt("PendingId");
                        intentToService.putExtra("ON_OFF", "ADD");
                        context.startService(intentToService);
                        break;
                    case "OFF_INTENT":
                        // get alarm'id from extras
                        int alarmId = intent.getExtras().getInt("AlarmId");
                        // sending to AlarmService
                        intentToService.putExtra("ON_OFF", "OFF");
                        intentToService.putExtra("AlarmId", alarmId);
                        context.startService(intentToService);
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}

package ngocvuthe.asm3.android.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.util.ArrayList;

import ngocvuthe.asm3.android.model.Alarm;
import ngocvuthe.asm3.android.R;

public class AlarmAdapter extends BaseAdapter {

    private ArrayList<Alarm> alarms;

    private Context context;

    private CallBack callBack;

    public AlarmAdapter(ArrayList<Alarm> alarms, Context context, CallBack callBack) {
        this.alarms = alarms;
        this.context = context;
        this.callBack = callBack;
    }

    @Override
    public int getCount() {
        return alarms.size();
    }

    @Override
    public Alarm getItem(int position) {
        return alarms.get(position);
    }

    @Override
    public long getItemId(int position) {
        return alarms.get(position).getId();
    }

    public void add(Alarm alarm) {
        alarms.add(alarm);
    }

    public ArrayList<Alarm> getAlarms() {
        return alarms;
    }

    public void removeAlarm(int position) {
        //TODO: Xử lý logic xóa alarm khỏi adapter
        try {
            alarms.remove(position);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateAlarm(Alarm alarm, int position) {
        alarms.remove(position);
        alarms.add(position, alarm);
    }

    public interface CallBack {
        //Callback xử lý logic start alarm
        void startAlarm(Alarm timeItem);

        //Callback xử lý logic cancel alarm
        void cancelAlarm(Alarm timeItem);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater inf = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inf.inflate(R.layout.alarm_item, null);

            holder = new ViewHolder();
            holder.time_Alarm =  convertView.findViewById(R.id.time_Alarm);
            holder.toggle_Alarm =  convertView.findViewById(R.id.toggle_Alarm);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.time_Alarm.setText(getStringFromTime(alarms.get(position)));

        int onOff = alarms.get(position).getFlag();
        switch (onOff) {
            case 1:
                holder.toggle_Alarm.setChecked(true);
                // this set color for time text green if toggle on on the bind view time
                holder.time_Alarm.setTextColor(Color.parseColor("#9be7ae"));
                break;
            case 0:
                holder.toggle_Alarm.setChecked(false);
                // this set color for time text green if toggle on on the bind view time
                holder.time_Alarm.setTextColor(Color.parseColor("#646161"));
                break;
        }


        final ViewHolder finalHolder = holder;
        holder.toggle_Alarm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    finalHolder.time_Alarm.setTextColor(Color.parseColor("#9be7ae"));
                    callBack.startAlarm(alarms.get(position));
                } else {
                    finalHolder.time_Alarm.setTextColor(Color.parseColor("#646161"));
                    callBack.cancelAlarm(alarms.get(position));
                }
            }
        });

        return convertView;
    }

    private String getStringFromTime(Alarm alarm) {
        int hour = alarm.getHour();
        int minute = alarm.getMinute();
        String textHour = String.valueOf(hour);
        String textMinute = String.valueOf(minute);
        String format = "PM";

        if(hour < 12){

            if(hour < 10) {
                textHour = "0" + hour;
            }

            format = "AM";
        }

        if(minute < 10) {
            textMinute = "0" + minute;
        }

        return textHour + " : " + textMinute + " - " + format;
    }

    private static class ViewHolder {
        public TextView time_Alarm;
        public ToggleButton toggle_Alarm;
    }

}

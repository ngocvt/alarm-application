package ngocvuthe.asm3.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.appcompat.app.AppCompatActivity;

import ngocvuthe.asm3.android.model.Alarm;
import ngocvuthe.asm3.android.R;

public class AddAlarmActivity extends AppCompatActivity {

    Toolbar toolbarAdd;
    TimePicker timePicker;
    Button btnCreateAlarm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        btnBack();
        createAlarm();
    }

    private void btnBack() {
        toolbarAdd = findViewById(R.id.toolBarAdd);
        toolbarAdd.setNavigationIcon(R.drawable.back);

        toolbarAdd.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(2);
                onBackPressed();
            }
        });
    }

    private Alarm initAlarm() {

        timePicker = findViewById(R.id.timePicker);

        int flag = 1;
        Alarm alarm;
        // get current time from timePicker
        int hour = 0;
        int minute = 0;

        try {
            hour = timePicker.getCurrentHour();
            minute = timePicker.getCurrentMinute();
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        alarm = new Alarm(hour, minute, flag);

        return alarm;
    }

    private void createAlarm() {
        btnCreateAlarm = findViewById(R.id.addAlarm);

        btnCreateAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddAlarmActivity.this, MainActivity.class);
                Alarm alarm = initAlarm();
                alarm.setId((int) System.currentTimeMillis());
                intent.putExtra("ALARM", "ADD_ALARM");
                intent.putExtra("ADD_ALARM", alarm);
                setResult(MainActivity.RESULT_OK, intent);
                finish();
            }
        });
    }

}

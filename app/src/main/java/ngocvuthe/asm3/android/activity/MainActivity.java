package ngocvuthe.asm3.android.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;

import ngocvuthe.asm3.android.model.Alarm;
import ngocvuthe.asm3.android.receiver.AlarmReceiver;
import ngocvuthe.asm3.android.R;
import ngocvuthe.asm3.android.adapter.AlarmAdapter;
import ngocvuthe.asm3.android.data.DatabaseManager;

public class MainActivity extends AppCompatActivity implements AlarmAdapter.CallBack {

    Button btnAddAlarm;
    ListView listView;
    private AlarmAdapter alarmAdapter;
    private DatabaseManager databaseManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViewAddAlarm();
        setAllTime();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);


        if (resultCode == RESULT_OK) {
            String checkType = (String) data.getSerializableExtra("ALARM");

            switch (checkType) {
                case "ADD_ALARM":

                    Alarm addAlarm = (Alarm) data.getSerializableExtra("ADD_ALARM");

                    boolean alarmExist = checkAlarm(addAlarm);

                    if (!alarmExist) {
                        alarmAdapter.add(addAlarm);

                        alarmAdapter.notifyDataSetChanged();

                        databaseManager.insert(addAlarm);

                        setAlarm(addAlarm, 0);

                        Toast.makeText(this, "Created", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case "EDIT_ALARM":
                    Alarm editAlarm = (Alarm) data.getSerializableExtra("EDIT_ALARM");
                    int position = (int) data.getSerializableExtra("position");
                    boolean checkAlarm = checkAlarm(editAlarm);

                    if (!checkAlarm) {
                        alarmAdapter.updateAlarm(editAlarm, position);

                        alarmAdapter.notifyDataSetChanged();

                        databaseManager.update(editAlarm);

                        setAlarm(editAlarm, 0);

                        Toast.makeText(this, "Alarm Changed", Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
        }

    }


    private void initViewAddAlarm() {
        btnAddAlarm = findViewById(R.id.btnAddAlarm);

        btnAddAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int LAUNCH_SECOND_ACTIVITY = 1;
                Intent intent = new Intent(MainActivity.this, AddAlarmActivity.class);
                startActivityForResult(intent, LAUNCH_SECOND_ACTIVITY);
            }
        });
    }

    private void setAllTime() {
        listView = findViewById(R.id.allTime);

        getAllAlarmList();

        listView.setAdapter(alarmAdapter);

        listView.setClickable(false);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int LAUNCH_SECOND_ACTIVITY = 1;
                Intent intent = new Intent(MainActivity.this, EditAlarmActivity.class);
                Alarm alarm = alarmAdapter.getItem(position);
                intent.putExtra("EDIT_ALARM", alarm);
                intent.putExtra("position", position);
                setResult(EditAlarmActivity.RESULT_OK, intent);
                startActivityForResult(intent, LAUNCH_SECOND_ACTIVITY);
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                if (alarmAdapter.getAlarms().size() > 0) {
                    final AlertDialog.Builder adb = new AlertDialog.Builder(MainActivity.this);
                    adb.setTitle("Delete?");
                    adb.setMessage("Are you sure you want to delete this alarm ?");
                    adb.setNegativeButton("Cancel", null);
                    adb.setPositiveButton("Ok", new AlertDialog.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Alarm alarm = alarmAdapter.getItem(position);
                            databaseManager.delete(alarm.getId());
                            alarmAdapter.removeAlarm(position);
                            alarmAdapter.notifyDataSetChanged();
                        }
                    });
                    adb.show();
                }
                return true;
            }
        });

    }

    private void getAllAlarmList() {

        if (alarmAdapter == null) {

            databaseManager = new DatabaseManager(this);

            ArrayList<Alarm> arrayList = databaseManager.getAlarmList();

            alarmAdapter = new AlarmAdapter(arrayList, this, MainActivity.this);
        }
    }


    private void setAlarm(Alarm alarm, int flags) {

        Calendar myCalendar = Calendar.getInstance();

        Calendar calendar = (Calendar) myCalendar.clone();

        calendar.set(Calendar.HOUR_OF_DAY, alarm.getHour());

        calendar.set(Calendar.MINUTE, alarm.getMinute());

        calendar.set(Calendar.SECOND, 0);

        if (calendar.compareTo(myCalendar) <= 0) {
            calendar.add(Calendar.DATE, 1);
        }

        int alarmId = alarm.getId();

        Intent intent = new Intent(MainActivity.this, AlarmReceiver.class);

        intent.putExtra("intentType", "ADD_INTENT");

        intent.putExtra("PendingId", alarmId);

        PendingIntent alarmIntent = PendingIntent.getBroadcast(MainActivity.this, alarmId,
                intent, flags);

        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP,
                calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, alarmIntent);
    }

    @Override
    public void startAlarm(Alarm alarmItem) {
        //set flag = 1 to start alarm
        alarmItem.setFlag(1);
        //update alarm in db
        databaseManager.update(alarmItem);
        //set alarm
        setAlarm(alarmItem, 0);
        //show notification
        Toast.makeText(this, "Alarm is on", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void cancelAlarm(Alarm alarmItem) {
        //set flag = 1 to cancel alarm
        alarmItem.setFlag(0);
        // update alarm in db
        databaseManager.update(alarmItem);
        // cancel this Alarm PendingIntent
        deleteCancel(alarmItem);
        // if alarm is triggered and ringing, send this alarm to AlarmReceiver
        // then AlarmReceiver send to service to stop music
        sendIntent(alarmItem, "OFF_INTENT");
        //show notification
        Toast.makeText(this, "Alarm is off", Toast.LENGTH_SHORT).show();
    }

    private void deleteCancel(Alarm alarm) {
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

        int alarmId = alarm.getId();

        Intent intent = new Intent(MainActivity.this, AlarmReceiver.class);
        intent.putExtra("intentType", "OFF_INTENT");

        PendingIntent alarmIntent = PendingIntent.getBroadcast(MainActivity.this, alarmId, intent, 0);

        alarmManager.cancel(alarmIntent);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void sendIntent(Alarm alarm, String intentType) {

        Intent intent1 = new Intent(MainActivity.this, AlarmReceiver.class);

        intent1.putExtra("intentType", intentType);

        intent1.putExtra("AlarmId", alarm.getId());

        sendBroadcast(intent1);
    }

    private boolean checkAlarm(Alarm alarm) {
        boolean contain = false;
        for (Alarm alarm1 : alarmAdapter.getAlarms()) {
            if (alarm1.getHour() == alarm.getHour() && alarm1.getMinute() == alarm.getMinute())
                contain = true;
        }

        if (contain) {
            Toast.makeText(this, "You already set this Alarm", Toast.LENGTH_SHORT).show();
        }

        return contain;
    }
}

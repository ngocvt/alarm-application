package ngocvuthe.asm3.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.appcompat.app.AppCompatActivity;

import ngocvuthe.asm3.android.model.Alarm;
import ngocvuthe.asm3.android.R;

public class EditAlarmActivity extends AppCompatActivity {

    Toolbar toolbarEdit;
    Button btnSaveAlarm;
    TimePicker timePicker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        btnBack();
        saveAlarm();
    }

    private void btnBack() {
        toolbarEdit = findViewById(R.id.toolBarEdit);
        toolbarEdit.setNavigationIcon(R.drawable.back);
        toolbarEdit.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(2);
                onBackPressed();
            }
        });
    }

    private Alarm initAlarm() {

        timePicker = findViewById(R.id.timePicker);

        int flag = 1;
        Alarm alarm;
        // get current time from timePicker
        int hour = 0;
        int minute = 0;

        try {
            hour = timePicker.getCurrentHour();
            minute = timePicker.getCurrentMinute();
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        alarm = new Alarm(hour, minute, flag);

        return alarm;
    }

    private void saveAlarm() {
        timePicker = findViewById(R.id.timePicker);
        Bundle data = getIntent().getExtras();

        final Alarm alarmEdit = (Alarm) data.getSerializable("EDIT_ALARM");
        final int position =  (int) data.getSerializable("position");
        timePicker.setHour(alarmEdit.getHour());

        timePicker.setMinute(alarmEdit.getMinute());


        btnSaveAlarm = findViewById(R.id.save);

        btnSaveAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EditAlarmActivity.this, MainActivity.class);
                Alarm alarm = initAlarm();

                int hour = alarm.getHour();
                int minute = alarm.getMinute();

                alarmEdit.setHour(hour);
                alarmEdit.setMinute(minute);

                intent.putExtra("ALARM", "EDIT_ALARM");
                intent.putExtra("EDIT_ALARM", alarmEdit);
                intent.putExtra("position", position);
                setResult(MainActivity.RESULT_OK, intent);
                finish();
            }
        });
    }
}
